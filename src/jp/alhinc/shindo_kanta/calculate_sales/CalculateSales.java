package jp.alhinc.shindo_kanta.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchName = new HashMap<>();
		Map<String,Long> totalSale = new HashMap<String,Long>();
		if(args.length != 1 ) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if(!dateInput(args[0],"branch.lst", branchName, totalSale,"^[0-9]{3}","支店")){
			return;
		}
		if(!dateCalculation(args[0],branchName, totalSale)){
			return;
		}
		if(!dateOutput(args[0],"branch.out",branchName, totalSale)){
			return;
		}
	}


	public static boolean dateInput(String inputPass,String filepass,Map<String, String> branchName,Map<String,Long> totalSale,String conditions,String name){
		BufferedReader in = null;
		FileReader fin =null;
		String[] fruit = new String[100];
		String line ;

		try {
			File file = new File(inputPass,filepass);
			if(!file.exists()) {
				System.out.println(name+"定義ファイルが存在しません");
				return false;
			}
			fin = new FileReader(file);
			in = new BufferedReader(fin);
			while((line = in.readLine()) != null) {
				fruit = line.split(",", 0);
				if(fruit[0].matches(conditions) && fruit.length == 2) {
					branchName.put(fruit[0],fruit[1]);
					totalSale.put(fruit[0],0L);
				}else {
					System.out.println(name+"定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (fin != null) {
					fin.close();
				}
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}return true;
	}

	public static boolean dateCalculation(String inputPass,Map<String, String> branchName,Map<String,Long> totalSale){
		FileReader fin =null;
		BufferedReader in = null;
		try {
			String line ;
			List<String> list1 = new ArrayList<String>();
			File cdirectory = new File(inputPass);
			String filelist[] = cdirectory.list();
			for (int i = 0 ; i < filelist.length ; i++) {
				File check = new File(inputPass,filelist[i]);
				if(filelist[i].matches("^[0-9]{8}.rcd$") && check.isFile()) {
					list1.add(filelist[i]);
				}
			}
			for(int i= 0; i<list1.size()-1; i++) {
				String result = list1.get(i).substring(0,8);
				String result1 = list1.get(i+1).substring(0,8);
				int value = Integer.parseInt(result);
				int value1 = Integer.parseInt(result1);
				if(value1 - value != 1){
					System.out.println("売上ファイル名が連番になっていません");
					return false;
				}
			}
			for(int l= 0; l<list1.size(); l++) {
				File cdirectory1 = new File(inputPass,list1.get(l));
				fin = new FileReader(cdirectory1);
				in = new BufferedReader(fin);
				List<String> list = new ArrayList<String>();
				while((line = in.readLine()) != null) {
					list.add(line);
				}
				if(list.size() != 2){
					System.out.println(list1.get(l)+"のフォーマットが不正です");
					return false;
				}
				if(!list.get(1).matches("^[0-9]*$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				if(!totalSale.containsKey(list.get(0))){
					System.out.println(list1.get(l)+"の支店コードが不正です");
					return false;
				}
				long initial = Long.parseLong(list.get(1));
				long calculation = totalSale.get(list.get(0)) + initial;
				if(calculation>9999999999L){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				totalSale.put(list.get(0), calculation);
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			try {
				if (in != null) {
					in.close();
				}
				if (fin != null) {
					fin.close();
				}
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}return true;
	}

	public static boolean dateOutput(String outpass,String filepass,Map<String, String> branchName,Map<String,Long> totalSale){
		BufferedWriter out = null;
		try{
			File file = new File(outpass,filepass);
			FileWriter fw = new FileWriter(file);
			out = new BufferedWriter(fw);
			for(Map.Entry<String, String> branchTotal : branchName.entrySet()) {
				out.write(branchTotal.getKey() + "," + branchTotal.getValue() + "," + totalSale.get(branchTotal.getKey()));
				out.newLine();
			}
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			try{
				if (out != null) {
					out.close();
				}
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}return true;
	}
}

